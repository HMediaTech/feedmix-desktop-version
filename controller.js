  var lastUrl = '';
  var mainurl = 'http://52.232.43.104:8080/feedmix/restservice';

  var app = angular.module("FeedMix", ['ngRoute', 'jcs-autoValidate']);
  
  
  app.config(['$routeProvider', function($routeProvider){

    $routeProvider
    .when('/', {
      templateUrl:'login.html',
      controller:'LoginCtrl'
    })
    .when('/settings', {
      templateUrl:'settings.html',
      controller:'SettingsCtrl'
    })
    .when('/posts',{
      templateUrl: 'feedMix.html',
      controller: 'PostsCtrl'
    })
    .otherwise({
      redirectTo: '/'
    });
  }]);
  
  app.run(function($rootScope,$location, User) {
    $rootScope.$on("$locationChangeStart", function(event, next, current){
      console.log(event, next, current);
      if(!User.isLogged()){
                 $location.path('/');
               }

    });
  });

  app.service('User', function (){
   this.login=false;
   this.saveToken = function(value){
    localStorage.setItem('token', value);
    this.login=true;
   }
   this.getToken = function(){
    return localStorage.getItem('token');
   }
   this.logout = function(){
    localStorage.removeItem('token');
    this.login=false; 
   }
    this.isLogged = function(){
      if(localStorage.getItem('token')){
        this.login=true;
      }
    return this.login;
   }
  });

  app
  .filter('removeHTMLTags', function () { 
    return function (text) {
      return text ? String(text).replace(/<[^>]+>/gm, '') : ''; 
    };
  })
  .filter('removeBackSlash', function () { 
    return function (text) {
      return text ? String(text).replace(/\\/g, '') : '';
    };
  })
  .filter('addHTTP', function () { 
    return function (text) {
      text = "https://" + text;
      //console.log(text);
      return text;
    };
  })
 .controller('PostsCtrl', function($scope, $http, User, $location) {
    
    $scope.empty = false;
    var access_token = User.getToken('token');

     $scope.logout=function(e){
        e.preventDefault();
        User.logout();
         $location.path('/');
      }

    // $scope.redirect = function(){
    //    $rootScope.loggedIn=true;
    //    $location.path('/posts');
    // };
  var postGetUrl = "http://52.232.43.104:8080/feedmix/restservice/posts/desktop/"+access_token;
  console.log(postGetUrl);
    $http.get(postGetUrl).then(function(response) {  
          $scope.empty=false;
          if(response.data=='undefined' || response.data.length==0){
            $scope.empty=true;
          } else
          {   
            $scope.empty=false;
            $scope.users = response.data;
          }
            // refresh();
          initIsotope();
      }, function(e){
        User.logout();
        $location.path('/');
      });

        // angular.element(document).ready(function () {
        //flip on show
             setInterval(function(){
                $http.get(postGetUrl).then(function(posts){
                    if (posts.data[0].url != lastUrl) {
                        lastUrl = posts.data[0].url;
                        $scope.users = posts.data;
                        initIsotope();
                    }
                },function(error){
                User.logout();
                $location.path('/');
             })

            }, 60000);
        // });

  });

  
  app.controller('MainCtrl' , function($scope, $location){
    $scope.setRoute=function(route){
      $location.path(route);
    }
  });

  // load landing page
  app.controller('LoginCtrl', ['$http', '$scope', '$location', '$rootScope', 'User' ,
                               function($http, $scope, $location, $rootScope, User){
    
    $scope.user=false;
    $scope.registered=false;
    $scope.logged=true;

     //get values to nested login in scope
     $scope.login = {
      email: '',
      password: ''
     }

     if(User.isLogged()){
      $location.path('/posts');
     }

     // $rootScope.loggedIn=true;
     //  $location.path('/posts');

     $scope.submit=function(e){
      e.preventDefault();
       $scope.logged=false;

       var loginlink='http://52.232.43.104:8080/feedmix/restservice/login';

       var data=JSON.stringify($scope.login);

           $http.post(loginlink,data).success(function(response){
            
            console.log(response);
             if(response.data.access_token!=='false' && response.oauth!=='false'){
                 User.saveToken(response.data.access_token);
                $location.path('/posts');
               // console.log(User.getItem());

             }
             else{
              $location.path('/');
              $scope.user=true;
              $scope.logged=true;
             }
              
           }, function(error){
           User.logout();
            $location.path('/');
            scope.logged=true;
          });
        }

   }]);
  
  //settings controller 
  app.controller('SettingsCtrl', ['$scope', 'User', '$http', '$location', function($scope, User, $http, $location){
      var token=User.getToken();

      $scope.processing=false;
      $scope.wait=false;

      var geturl = 'http://52.232.43.104:8080/feedmix/restservice/settings/'+token;
      
       $scope.keypressSetting = function(e) {
          if(e.keyCode==13){
                e.preventDefault();
               }
         };

      $scope.namehide = true;
      $scope.surnamehide = true;
      $scope.emailhide = true;
      $scope.invalid = false;
      $scope.valid = false;

      $http.get(geturl).then(function(response) {   

          $scope.username = response.data.data.user_profile.name;
          $scope.usersurname = response.data.data.user_profile.surname;
          $scope.useremail = response.data.data.user_profile.email;
          $scope.followers = response.data.data.user_profile.followers;

          for (var i = 0; i < response.data.data.user_profile.followers.accounts.length; i++) {
            $('#useraccounts').tagsinput('add', response.data.data.user_profile.followers.accounts[i]);
          };
          
          for(var i=0; i< response.data.data.user_profile.followers.reddit.subreddits.length; i++){
           $('#usersubs').tagsinput('add', response.data.data.user_profile.followers.reddit.subreddits[i]);
          };

          for(var i=0; i< response.data.data.user_profile.followers.reddit.queries.length; i++){
           $('#userqueries').tagsinput('add', response.data.data.user_profile.followers.reddit.queries[i]);
          };

          for(var i=0; i< response.data.data.user_profile.followers.rss.length; i++){
           $('#userlinks').tagsinput('add', response.data.data.user_profile.followers.rss[i]);
          };

      });


      // $scope.form = {
      //   name: $scope.settings.name,
      //   surname: $scope.settings.surname,
      //   email: $scope.settings.email
      // }

      $scope.edition = {
        name: $scope.username,
        surname: $scope.usersurname, 
        email: $scope.useremail
      }
      
      $scope.editname = function(e, v) {
        e.preventDefault();
        $scope.namehide = false;
        $scope.edition.name=v;
      }
      
      $scope.editsurname = function(e, v) {
        e.preventDefault();
        $scope.surnamehide = false;
        $scope.edition.surname = v;
      }
      
      $scope.editemail = function(e, v) {
        e.preventDefault();
        $scope.emailhide = false;
        $scope.edition.email = v;
      }

      $scope.savename = function(e){
        e.preventDefault();
        $scope.username = $scope.edition.name;
        $scope.namehide = true;
      }
      
      $scope.savesurname = function(e){
        e.preventDefault();
        $scope.usersurname = $scope.edition.surname;
        $scope.surnamehide = true;
      }
      
      $scope.saveemail = function(e){
        e.preventDefault();
        $scope.useremail = $scope.edition.email;
        $scope.emailhide = true;
      }

      $scope.submit = function(e){
        e.preventDefault();

      }

      $scope.cancelname = function(e){
        e.preventDefault();
        $scope.edition.name = '';        
        $scope.namehide = true;
      }

      $scope.cancelsurname = function(e){
        e.preventDefault();
        $scope.edition.surname = '';       
        $scope.surnamehide = true;
      }

      $scope.cancelemail = function(e){
        e.preventDefault();
        $scope.edition.email = '';
        $scope.emailhide = true;
      }

      $('#useraccounts').tagsinput({
        confirms: [13,188]
      });

      $('#userhashtags').tagsinput({
        confirms: [13,188]
      });
    

      $('#usersubs').tagsinput({
        confirms: [13,188]
      });
      

      $('#userqueries').tagsinput({
        confirms: [13,188]
      });
      

      $('#userlinks').tagsinput({
        confirms: [13,188],
        source:["test"]
      });
      

      $scope.submit=function(e){
        e.preventDefault();

        $scope.processing=true;
        $scope.wait=true;
        $scope.valid=false;
        $scope.invalid=false;


        var newtoken = User.getToken();
        var updateurl='http://52.232.43.104:8080/feedmix/restservice/settings/update/'+newtoken;
        var useraccounts=$('#useraccounts').tagsinput('items');
        var userhashtags=$('#userhashtags').tagsinput('items');
        var usersubs=$('#usersubs').tagsinput('items');
        var userqueries=$('#userqueries').tagsinput('items');
        var userlinks=$('#userlinks').tagsinput('items');
       
        var data={
            name:$scope.username,
            surname:$scope.usersurname,
            email:$scope.useremail,
            followers:{
                accounts:useraccounts,
                reddit:{
                    subreddits:usersubs,
                    queries:userqueries
                }, 
                rss:userlinks
            }
        }

        $http.post(updateurl,data).success(function(response){
          $scope.processing=false;
          $scope.wait=false;
            // $location.path('#/settings');
            if(!response.data.feedback.accounts.length==0 || !response.data.feedback.subreddits.length==0 
              || !response.data.feedback.rsslinks.length==0){
              $scope.invalid=true;
              $scope.valid=false;
              $scope.accounts=response.data.feedback.accounts;
              $scope.subs = response.data.feedback.subreddits;
              $scope.rsslinks = response.data.feedback.rsslinks;
            }
            else{
              $scope.valid=true;
              $scope.invalid=false;
            }

        }, function(error){
           User.logout();
            $location.path('/');
        })
      }
      
     
  }]);

  //registration controller
  app.controller('RegistrationCtrl', function($scope, $http, $location, User){
   
   $scope.registered = false;
   $scope.registration = true;

   $scope.keypress = function(e) {
    if(e.keyCode==13){
          e.preventDefault();
         }
   };
   
   $('#accounts').tagsinput({ confirms:[13,188]} );
   
    // $('#hashtags').tagsinput({confirms:[13,188]});
   
   $('#subs').tagsinput({confirms:[13,188]});
   
   $('#queries').tagsinput({confirms:[13,188]});
   
   $('#links').tagsinput({confirms:[13,188]});
    

   $scope.press = function(e) {

    e.preventDefault();
    // $scope.isRegistered = true;

    var accounts=$('#accounts').tagsinput('items');
    // var hashtags=$('#hashtags').tagsinput('items');
    var subs=$('#subs').tagsinput('items');
    var queries=$('#queries').tagsinput('items');
    var links=$('#links').tagsinput('items');
    
    $scope.keypress = function(e) {
    if(e.keyCode==13){
          e.preventDefault();
         }
    };

    $scope.registration=false;

    var regurl='http://52.232.43.104:8080/feedmix/restservice/registration';

    var data={
            name:$scope.name,
            surname:$scope.surname,
            email:$scope.email,
            password:$scope.password,
            followers:{
                accounts:accounts,
                reddit:{
                    subreddits:subs,
                    queries:queries
                }, 
                rss:links
            }
        }

        $http.post(regurl,data).success(function(response){
          $scope.registration=true;
          $scope.registered=true;
        }).error(function(e){
            User.logout();
         $location.path('/');
        });
    };

  });



//Javascript for modal

//   $('#myModal').on('show.bs.modal', function (event) {
//     console.log("willysssss")
//   var button = $(event.relatedTarget) // Button that triggered the modal
//   var recipient = button.data('url')
//    // Extract info from data-* attributes
//   // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
//   // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
//   console.log(recipient);

//   var modal = $(this)
//   modal.find('.myModal').text('New message to ' + recipient)
//   modal.find('.modal-body input').val(recipient)
//   $('#info').attr('src', recipient);
// })

// $('#myModal').on('show.bs.modal', function (event) {
//     console.log("willysssss")
//   var button = $(event.relatedTarget) // Button that triggered the modal
//   var recipient = button.data('src')
//    // Extract info from data-* attributes
//   // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
//   // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
//   console.log(recipient);

//   var modal = $(this)
//   modal.find('.myModal').text('New message to ' + recipient)
//   modal.find('.modal-body input').val(recipient)
//   $('#source').attr('src', recipient);
// })





