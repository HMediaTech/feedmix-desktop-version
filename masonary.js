jQuery(document).ready(function () {setTimeout(function(){


    
  // Takes the gutter width from the bottom margin of .grid

  var gutter = parseInt(jQuery('.grid').css('marginBottom'));
  var container = jQuery('#grid-item');



  // Creates an instance of Masonry on #grid-item

  container.masonry({
    gutter: gutter,
    itemSelector: '.grid',
    columnWidth: '.grid'
  });
  
  
  
  // This code fires every time a user resizes the screen and only affects .grid elements
  // whose parent class isn't .container. Triggers resize first so nothing looks weird.
  
  jQuery(window).bind('resize', function () {
    if (!jQuery('#grid-item').parent().hasClass('container')) {
      
      
      
      // Resets all widths to 'auto' to sterilize calculations
      
      post_width = jQuery('.grid').width() + gutter;
      jQuery('#grid-item, body > #grid').css('width', 'auto');
      
      
      
      // Calculates how many .grid elements will actually fit per row. Could this code be cleaner?
      console.log('wang');
      posts_per_row = jQuery('#grid-item').innerWidth() / post_width;
      floor_posts_width = (Math.floor(posts_per_row) * post_width) - gutter;
      ceil_posts_width = (Math.ceil(posts_per_row) * post_width) - gutter;
      console.log('johnson');
      posts_width = (ceil_posts_width > jQuery('#grid-item').innerWidth()) ? floor_posts_width : ceil_posts_width;
      if (posts_width == jQuery('.grid').width()) {
        posts_width = '100%';
      }
      
      
      
      // Ensures that all top-level elements have equal width and stay centered
      
      jQuery('#grid-item, #grid').css('width', posts_width);
      jQuery('#grid').css({'margin': '0 auto'});
            
    
    
    }
  }).trigger('resize');
  
  }, 200);

});

// setTimeout(function(){
//  // your code here.

// var elem = document.querySelector('.grid');
// var msnry = new Masonry( elem, {
//   // options
//   itemSelector: '.grid-item',
//   columnWidth: 200
// });

// // element argument can be a selector string
// //   for an individual element
// var msnry = new Masonry( '.grid', {
//   // options
// });


// },100);