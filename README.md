# README #

This README would normally document whatever steps are necessary to get your application up and running.

### FeedMix Desktop Front End ###
A user interface version of FeedMix which displays aggrated tech related content from various sources 
and display it in one central hub. 
The front end is developed in HTML, CSS and AngulajJS. The desktop content is made of articles stored in cards in 
a grid formation that uses a libary called "isotope" for filtering the content with animations and also provides a responsive layout.

### How do I get set up? ###

* Clone repo to local file 
* Files are static so "feedMix.html" simply needs to be open to view project

### Issues ###

* There is an issue with the loading of the content and isotope gird simoustanly.  
* This a sublte issue but essentially there is a delay on the grid on page load/refresh.
* The masonary grid was also not working, its is possible it was conflicting with the isotope filtering also but this would be a nice feature moving forward. 



### Techincal Requirements ###
npm install angular
Bootstrap3